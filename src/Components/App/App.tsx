import React, { useState } from 'react';
import './App.css';
import { Card } from '../Card/Card';
import { shuffleItems } from '../../data';
import { ResultBlock } from '../ResultBlock/ResultBlock';

export const App = () => {
  const maxCount: number = 40;
  const allCardCount: number = shuffleItems.length;
  const allCardPair: number = allCardCount / 2;

  const [count, setCount] = useState<number>(0);
  const [activeId, setActiveId] = useState<number | null>(null);
  const [activeItemSrc, setActiveItemSrc] = useState<string | null>(null);
  const [isPair, setIsPair] = useState<boolean>(false);
  const [okPairs, setOkPairs] = useState<string[]>([]);

  const checkItem = (id: number, src: string) => {
    if (id === activeId) {
      return;
    }
    // Увеличиваем счетчик
    setCount(count + 1);
    // Запоминаем текущий выбор по id
    setActiveId(id);
    // Запоминаем текущую карту по значению в src
    setActiveItemSrc(src);
    // Проверяем пару по src
    setIsPair(activeItemSrc === src);
    // Сохраняем угаданные пары в массив
    activeItemSrc === src ? okPairs.push(src) : okPairs;
    setOkPairs(okPairs);
  };

  return (
    <div className="container">
      <h1 className="title">MEMORY</h1>
      <div className="grid box-grid">
        <div className="box-1">
          <div>
            <div className="result-text">сделано ходов</div>
            <div className="result-count">{count}</div>
          </div>
        </div>
        <div className="box-2">
          <div className="grid grid-field">
            {shuffleItems.map((item) => {
              const isWinValue: boolean = (activeItemSrc === item.src && isPair) || okPairs.includes(item.src);

              return (
                <Card
                  src={item.src}
                  id={item.id}
                  key={item.id}
                  isOpen={activeId === item.id}
                  isWin={isWinValue}
                  onClick={() => checkItem(item.id, item.src)}
                />
              );
            })}
          </div>
        </div>
        <div className="box-3">
          <div>
            <div className="result-text">осталось попыток</div>
            <div className="result-count">{maxCount - count}</div>
          </div>
        </div>
      </div>
      {okPairs.length === allCardPair ? <ResultBlock result="win" count={count} /> : null}

      {maxCount - count === 0 ? <ResultBlock result="lose" /> : null}
    </div>
  );
};
