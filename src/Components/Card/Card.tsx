import React, { FC } from 'react';
import './Card.css';

type TProps = {
  id: number;
  src: string;
  isOpen: boolean;
  isWin: boolean;
  onClick: (key: number, src: string) => void;
};

export const Card: FC<TProps> = ({ id, src, isOpen, isWin, onClick }) => {
  return (
    <div className={'card' + (isOpen || isWin ? ' opened' : '')} onClick={() => onClick(id, src)}>
      <img src={src} alt="" className={isWin ? 'hide' : ''} />
    </div>
  );
};
