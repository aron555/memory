import React, { FC } from 'react';
import './ResultBlock.css';

type TProps = {
  result: string;
  count?: number;
};

export const ResultBlock: FC<TProps> = ({ result, count }) => {
  return (
    <div className="result-block">
      <div className="result-block__text">
        {result === 'win' ? (
          <div>
            <div>Ура, ВЫ выиграли!</div>
            <div>это заняло {count} ходов</div>
          </div>
        ) : (
          <div>
            <div>УВЫ, ВЫ ПРОИГРАЛИ</div>
            <div>У ВАС КОНЧИЛИСЬ ХОДЫ</div>
          </div>
        )}
      </div>
      <div className="result-block__bottom">
        <button className="result-block__btn" onClick={() => location.reload()}>
          сыграть еще
        </button>
      </div>
    </div>
  );
};
