import React from 'react';
import { createRoot } from 'react-dom/client';
import './common.css';
import { App } from './Components/App/App';

const container = document.getElementById('root') as HTMLBaseElement;
const root = createRoot(container);

root.render(<App />);
