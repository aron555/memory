import item1 from './images/items/item1.svg';
import item2 from './images/items/item2.svg';
import item3 from './images/items/item3.svg';
import item4 from './images/items/item4.svg';
import item5 from './images/items/item5.svg';
import item6 from './images/items/item6.svg';
import item7 from './images/items/item7.svg';
import item8 from './images/items/item8.svg';

import { Cards } from './types';

const cardList: string[] = [item1, item2, item3, item4, item5, item6, item7, item8];

const doubleCardList: string[] = [...cardList, ...cardList];

const doubleCardListObj: Cards[] = doubleCardList.map((item, index) => {
  return { id: index, src: item };
});

//  Тасование Фишера — Йетса
function shuffle(array: Cards[]): Cards[] {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));

    [array[i], array[j]] = [array[j], array[i]];
  }

  return array;
}

export const shuffleItems: Cards[] = shuffle(doubleCardListObj);
